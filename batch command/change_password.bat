@echo off
set /p newPassword="请输入新密码: "
net user %USERNAME% %newPassword%
if %ERRORLEVEL% == 0 (
    echo 密码修改成功。
) else (
    echo 密码修改失败，请确保你拥有足够的权限，并且新密码符合安全策略。
)
pause
