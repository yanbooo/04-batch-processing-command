@echo off
echo Stopping Print Spooler Service...
net stop Spooler

echo Deleting print jobs...
del /Q /F %windir%\System32\spool\PRINTERS\*.*
if %ERRORLEVEL% neq 0 (
    echo Failed to delete print jobs. Please check if the Print Spooler service is stopped and you have sufficient permissions.
    pause
    exit
)

echo Starting Print Spooler Service...
net start Spooler

echo Operation completed successfully.
pause
