@echo off
echo Releasing IP address...
ipconfig /release
echo.
echo Waiting for 10 seconds...
timeout /t 10 /nobreak
echo.
echo Renewing IP address...
ipconfig /renew
echo.
echo IP address has been renewed.
