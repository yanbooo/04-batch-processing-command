@echo off
echo 正在设置所有网络适配器自动获取IP地址和DNS服务器地址...

REM 设置网络适配器自动获取IP地址和DNS
netsh interface ip set address "本地连接" dhcp
netsh interface ip set dns "本地连接" dhcp
netsh interface ip set address "以太网" dhcp
netsh interface ip set dns "以太网" dhcp

netsh interface ip set address "无线网络连接" dhcp
netsh interface ip set dns "无线网络连接" dhcp
netsh interface ip set address "WLAN" dhcp
netsh interface ip set dns "WLAN" dhcp

REM 刷新DNS缓存
echo 正在刷新DNS缓存...
ipconfig /flushdns

REM 显示当前获取到的IP地址
echo 当前IP配置信息:
ipconfig /all

echo 完成设置。
pause
