@echo off
echo ###重要提示###
echo 这个脚本将执行网络修复操作，可能会暂时中断你的网络连接。
echo 在运行之前，请确保保存所有重要文件并关闭所有程序。
pause

echo 开始执行网络修复操作...

echo 重置Winsock...
netsh winsock reset

echo 重置IP堆栈...
netsh int ip reset

echo 更新自动获取的IP地址...
ipconfig /release
ipconfig /renew

echo 清除DNS缓存...
ipconfig /flushdns

echo 清理ARP缓存...
arp -d *

echo 网络修复操作完成。
echo 计算机将在一分钟后自动重启以应用所有更改并完成修复过程。
shutdown -r -t 60

echo 如果你想取消重启，请立即运行 shutdown -a 命令。
pause