@echo off
@echo //////////////////////////////////////////////////
@echo Copyright (C) 2012 DesktopCal, Inc.
@echo.
@echo [Notice]
@echo YOU MUST RUN THIS PROGRAM AS AN ADMINISTRATOR!
@echo.
@echo Please wait a moment while repairing ...
@echo.
@echo.
@echo.

net stop Spooler

del /F/Q "%windir%\System32\spool\PRINTERS\*.*"

net start Spooler

@echo Done!
pause